# https://www.pythonf.cn/read/89015

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from myExperiment_UI import Ui_MainWindow
import cv2
import numpy as np
import os, sys
from os import walk
import time
# import pymysql
import pandas as pd


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        desktop = QApplication.desktop()
        widget = QWidget()
        self.setGeometry(desktop.screenGeometry(3))
        self.setStyleSheet("#MainWindow{background-color: gray}")
        self.setupUi(self)
        # self.left = 0
        # self.top = 0
        # self.width = 1920
        # self.height = 1080
        self.initUI()
        self.setWindowFlags(Qt.CustomizeWindowHint)
        self.showMaximized()
        

    def Setup(self):
        self.ImageIndex = 0
        self.OriginalImagePath = "./Original/"
        self.ResultImagePath = "./Result800x800/"
        self.FontType = "微軟正黑體"
        


    def initUI(self):
        self.Setup()
        # self.setGeometry(self.left, self.top, self.width, self.height)
        self.ResultImage = myLabel(self)
        self.ResultImage.resize(800,800)
        self.ResultImage.move(610,120)
        self.ResultImage.setCursor(QCursor(Qt.PointingHandCursor))
        self.ResultImage.setFrameShape(QFrame.Box)
        self.ResultImage.setFrameShadow(QFrame.Raised)
        self.UserName.setPlaceholderText("請輸入中文姓名")
        self.ShowTooltip()
        # self.ResultImage.setScaledContents(True)
        self.ShowOriginalImage()
        self.ShowResultImage()
        self.NextButton.setText('下一步\n{}/{}'.format(self.ImageIndex + 1, len(self.OriginalImageFiles)))
        self.NextButton.setEnabled(False)
        self.ResultImage._signal.connect(self.ShowSubWindow)
        self.ResultImage._signal.connect(self.test)
        option = QGraphicsOpacityEffect()
        option.setOpacity(0)
        self.ExitButton.setGraphicsEffect(option)
        self.sub_window = SubWindow()
        self.ButtonClickEvent()
        self.Setup()


    def ButtonClickEvent(self):
        self.ExitButton.clicked.connect(QCoreApplication.instance().quit)
        # self.NextButton.clicked.connect(self.sub_window.show)
        
        self.InitFontSize = 20
        self.NextButton.clicked.connect(self.CountAllAnswer)
        self.LockButton.clicked.connect(self.LockName)
        self.OptionOneButton_1.clicked.connect(lambda:self.ChangeOptionValue_1(self.OptionOneButton_1))
        self.OptionOneButton_2.clicked.connect(lambda:self.ChangeOptionValue_1(self.OptionOneButton_2))
        self.OptionOneButton_3.clicked.connect(lambda:self.ChangeOptionValue_1(self.OptionOneButton_3))
        self.OptionOneButton_4.clicked.connect(lambda:self.ChangeOptionValue_1(self.OptionOneButton_4))
        self.OptionOneButton_5.clicked.connect(lambda:self.ChangeOptionValue_1(self.OptionOneButton_5))
        self.OptionTwoButton_1.clicked.connect(lambda:self.ChangeOptionValue_2(self.OptionTwoButton_1))
        self.OptionTwoButton_2.clicked.connect(lambda:self.ChangeOptionValue_2(self.OptionTwoButton_2))
        self.OptionTwoButton_3.clicked.connect(lambda:self.ChangeOptionValue_2(self.OptionTwoButton_3))
        self.OptionTwoButton_4.clicked.connect(lambda:self.ChangeOptionValue_2(self.OptionTwoButton_4))
        self.OptionTwoButton_5.clicked.connect(lambda:self.ChangeOptionValue_2(self.OptionTwoButton_5))
        self.OptionThreeButton_1.clicked.connect(lambda:self.ChangeOptionValue_3(self.OptionThreeButton_1))
        self.OptionThreeButton_2.clicked.connect(lambda:self.ChangeOptionValue_3(self.OptionThreeButton_2))
        self.OptionThreeButton_3.clicked.connect(lambda:self.ChangeOptionValue_3(self.OptionThreeButton_3))
        self.OptionThreeButton_4.clicked.connect(lambda:self.ChangeOptionValue_3(self.OptionThreeButton_4))
        self.OptionThreeButton_5.clicked.connect(lambda:self.ChangeOptionValue_3(self.OptionThreeButton_5))
        self.OptionFourButton_1.clicked.connect(lambda:self.ChangeOptionValue_4(self.OptionFourButton_1))
        self.OptionFourButton_2.clicked.connect(lambda:self.ChangeOptionValue_4(self.OptionFourButton_2))
        self.OptionFourButton_3.clicked.connect(lambda:self.ChangeOptionValue_4(self.OptionFourButton_3))
        self.OptionFourButton_4.clicked.connect(lambda:self.ChangeOptionValue_4(self.OptionFourButton_4))
        self.OptionFourButton_5.clicked.connect(lambda:self.ChangeOptionValue_4(self.OptionFourButton_5))

    def ChangeOptionValue_1(self, Score):
        self.OptionOneScore = int(Score.text())
        self.OptionGreat_1.setFont(QFont(self.FontType, self.OptionOneScore*2 + self.InitFontSize, QFont.Bold))
        self.OptionBad_1.setFont(QFont(self.FontType, self.InitFontSize - self.OptionOneScore*2, QFont.Bold))
        self.OptionScore_1.display(self.OptionOneScore)
        self.CheckNextButton()
        # print("完整度: ", self.OptionOneScore)

    def ChangeOptionValue_2(self, Score):
        self.OptionTwoScore = int(Score.text())
        self.OptionGreat_2.setFont(QFont(self.FontType, self.OptionTwoScore*2 + self.InitFontSize, QFont.Bold))
        self.OptionBad_2.setFont(QFont(self.FontType, self.InitFontSize - self.OptionTwoScore*2, QFont.Bold))
        self.OptionScore_2.display(self.OptionTwoScore)
        self.CheckNextButton()
        # print("銜接流暢度: ", self.OptionTwoScore)

    def ChangeOptionValue_3(self, Score):
        self.OptionThreeScore = int(Score.text())
        self.OptionGreat_3.setFont(QFont(self.FontType, self.OptionThreeScore*2 + self.InitFontSize, QFont.Bold))
        self.OptionBad_3.setFont(QFont(self.FontType, self.InitFontSize - self.OptionThreeScore*2, QFont.Bold))
        self.OptionScore_3.display(self.OptionThreeScore)
        self.CheckNextButton()
        # print("喜好度: ", self.OptionThreeScore)

    def ChangeOptionValue_4(self, Score):
        self.OptionFourScore = int(Score.text())
        self.OptionGreat_4.setFont(QFont(self.FontType, self.OptionFourScore*2 + self.InitFontSize, QFont.Bold))
        self.OptionBad_4.setFont(QFont(self.FontType, self.InitFontSize - self.OptionFourScore*2, QFont.Bold))
        self.OptionScore_4.display(self.OptionFourScore)
        self.CheckNextButton()
        # print("週期性: ", self.OptionFourScore)

    def ShowTooltip(self):
        self.OptionTitle_1.setToolTip('觀測者觀察所提示測試影像，在個人記憶中接近原物體的真實程度')
        self.OptionTitle_2.setToolTip('觀測者觀察所提示測試影像，其內部紋理線條，能否判斷相互自然連接的連續程度')
        self.OptionTitle_3.setToolTip('觀看整體圖片時，對於圖片的合成品質之滿意程度？')
        self.OptionTitle_4.setToolTip('對於影像有重複性的區塊出現時，其嚴重程度如何?')


    def ShowOriginalImage(self):
        # print(self.OriginalImagePath)
        self.OriginalImageFiles, path = self.check_Image_path(self.OriginalImagePath)
        # print(self.OriginalImageFiles)
        self.ShowOriginalImageInformation()
        im = QPixmap(path + self.OriginalImageFiles[self.ImageIndex])
        self.OriginalImage.setPixmap(im)

    def ShowResultImage(self):
        # print(self.ResultImagePath)
        self.ResultImageFiles, path = self.check_Image_path(self.ResultImagePath)
        # print(self.ResultImageFiles)
        self.ShowResultImageInformation()
        im = QPixmap(path + self.ResultImageFiles[self.ImageIndex])
        self.ResultImage.setPixmap(im)

    def ShowOriginalImageInformation(self):
        self.OriginalImageName.setText("Name : " + self.OriginalImageFiles[self.ImageIndex])
        img = cv2.imread(self.OriginalImagePath + self.OriginalImageFiles[self.ImageIndex])
        self.OriginalImageSize.setText("Size : " + str(img.shape))

    def ShowResultImageInformation(self):
        self.ResultImageName.setText("Name : " + self.ResultImageFiles[self.ImageIndex])
        img = cv2.imread(self.ResultImagePath + self.ResultImageFiles[self.ImageIndex])
        self.ResultImageSize.setText("Size : " + str(img.shape))

    def LockName(self):
        self.User = self.UserName.text()
        if self.LockButton.text() != "更改":
            self.UserName.setEnabled(False)
            self.LockButton.setText("更改")
            self.CheckNextButton()
            print("受測者: {}".format(self.User))
        else:
            self.UserName.setEnabled(True)
            self.LockButton.setText("鎖定")
            print("可更改名稱")

    def test(self):
        self.ResultImageFiles, path = self.check_Image_path(self.ResultImagePath)
        self.sub_window.ShowSubWindowResultImage(path, self.ResultImageFiles, self.ImageIndex)

    # 列出資料夾中的所有圖片
    def check_Image_path(self, path):
        if os.path.exists(path):
            for root, dirs, files in walk(path):
                files = sorted(files)
        else:
            print("請檢查路徑 !")
        return files, path

    # 檢查每個選項是否確實填寫
    def CheckNextButton(self):
        if (self.OptionScore_1.intValue() > 0 and self.OptionScore_2.intValue() > 0 and self.OptionScore_3.intValue() > 0 and self.OptionScore_4.intValue() > 0 and self.UserName.text() != ''):
            print("第{}題作答完畢".format(self.ImageIndex + 1))
            self.NextButton.setEnabled(True)
    
    def ShowSubWindow(self):
        self.sub_window.show()

    def CountAllAnswer(self):
        print("第{}張圖選擇的完整度為: {}".format(self.ImageIndex + 1, self.OptionOneScore))
        print("第{}張圖選擇的銜接流暢度為: {}".format(self.ImageIndex + 1, self.OptionTwoScore))
        print("第{}張圖選擇的喜好度為: {}".format(self.ImageIndex + 1, self.OptionThreeScore))
        print("第{}張圖選擇的週期性為: {}".format(self.ImageIndex + 1, self.OptionFourScore))
        self.ToExcel()
        if (self.ImageIndex + 1 < len(self.OriginalImageFiles)):
            # 儲存資料
            self.ImageIndex = self.ImageIndex + 1
            self.NextButton.setText('下一步\n{}/{}'.format(self.ImageIndex + 1, len(self.OriginalImageFiles)))
            # print(self.ImageIndex)
            self.ResetComponent()
            self.ShowOriginalImage()
            self.ShowResultImage()
            self.NextButton.setEnabled(False)
        else:
            self.FinishMessage()
            self.close()
            self.ImageIndex = 0


    # # mysql連線
    # def connection(self):
    #     try:
    #         conn = pymysql.connect(host='localhost',port = 3306,user='root',passwd='',db='texture_synthesis')
    #         cursor = conn.cursor()
    #         print("資料庫連接成功")
    #     except Exception as e:
    #         print("資料庫連接失敗:" + str(e))

    #     return conn, cursor


    # # 儲存資料到DB
    # def ToDataBase(self,data):
    #     conn, cursor = self.connection()
    #     insert_sql = "INSERT INTO data (name, picture_name, complete, smooth, preference, regular) VALUES (%s, %s, %s, %s, %s, %s);"
    #     insert_val = (data[0], data[1], data[2], data[3], data[4], data[5])

    #     cursor.execute(insert_sql, insert_val)
    #     data = cursor.fetchone()
    #     print("資料庫:{}".format(data))
    #     conn.commit()
    #     cursor.close()
    #     conn.close()


    # 寫入至Excel
    def ToExcel(self):
        excel_path = "./data.xlsx"
        df = pd.read_excel(excel_path)

        df_new = pd.DataFrame()
        df_new['編號'] = ["{}".format(self.ImageIndex)]
        df_new['受測者'] = ["{}".format(self.User)]
        df_new['圖片'] = ["{}".format(self.ResultImageFiles[self.ImageIndex])]
        df_new['完整度分數'] = ["{}".format(self.OptionOneScore)]
        df_new['銜接流暢度分數'] = ["{}".format(self.OptionTwoScore)]
        df_new['喜好度分數'] = ["{}".format(self.OptionThreeScore)]
        df_new['週期性分數'] = ["{}".format(self.OptionFourScore)]

        df_combine = df.append(df_new)
        df_combine.to_excel(excel_path, index=False)
        print("第{}題儲存成功".format(self.ImageIndex + 1))


    # 歸零
    def ResetComponent(self):
        self.OptionOneScore = 0
        self.OptionTwoScore = 0
        self.OptionThreeScore = 0
        self.OptionFourScore = 0
        self.OptionScore_1.display(0)
        self.OptionScore_2.display(0)
        self.OptionScore_3.display(0)
        self.OptionScore_4.display(0)


    # 對話框
    def FinishMessage(self):
        QMessageBox.information(self,"恭喜完成","感謝您的協助~!")

# 子視窗
class SubWindow(QWidget):
    def __init__(self):
        super(SubWindow, self).__init__()
        # self.resize(800, 800)
        # print(self.size())
        self.initUI()
        self.showMaximized()
        self.showFullScreen()
        self.setWindowFlags(Qt.CustomizeWindowHint)

    def initUI(self):
        self.desktop = QApplication.desktop()
        self.setGeometry(self.desktop.screenGeometry(1))
        self.screenRect = self.desktop.screenGeometry()
        self.height = self.screenRect.height()
        self.width = self.screenRect.width()
        print("子視窗解析度: {}x{}".format(self.width, self.height))
        self.FullScreenResultImage = myLabel(self)
        self.FullScreenResultImage.setGeometry(0, 0, self.width, self.height)
        # self.FullScreenResultImage.setScaledContents(True) # 讓圖片自適應label大小
        self.FullScreenResultImage.setText('Sub Window')
        self.FullScreenResultImage.setAlignment(Qt.AlignCenter)
        self.FullScreenResultImage.setStyleSheet('font-size:40px')
        self.FullScreenResultImage.setFrameShape(QFrame.Box)
        self.FullScreenResultImage.setFrameShadow(QFrame.Raised)
        self.FullScreenResultImage.setCursor(QCursor(Qt.PointingHandCursor))
        self.ButtonClickEvent()

    def ShowSubWindowResultImage(self, path, ResultImageFiles, ImageIndex):
        im = QPixmap(path + ResultImageFiles[ImageIndex])
        # pixmap = im.scaledToWidth(3840)
        # pixmap = im.scaledToHeight(2160)
        self.FullScreenResultImage.setPixmap(im)

    def ButtonClickEvent(self):
        self.FullScreenResultImage._signal.connect(self.CloseSubWindow)

    def CloseSubWindow(self):
        self.close()


class myLabel(QLabel):
    _signal = pyqtSignal(str)#信号定义
    def __init__(self, parent=None):
        super(myLabel, self).__init__(parent)

    def mousePressEvent(self, e):  ##重载一下鼠标点击事件
            self._signal.emit("正在檢測中")


if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        window = MainWindow()
        window.show()
        sys.exit(app.exec_())
    except KeyboardInterrupt:
        destory()