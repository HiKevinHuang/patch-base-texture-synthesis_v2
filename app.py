from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from UI.PyQTwindow import Ui_MainWindow
import cv2
import numpy as np
from controller.controller import RunTextureSynthesis
import sys, os
import time


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.left = 0
        self.top = 0
        self.width = 1920
        self.height = 1080
        self.initUI()

    def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.progressBar.hide()
        self.SaveButton.setEnabled(False)
        self.RunButton.setEnabled(False)
        infomation = ["週期影像","非週期影像"]
        self.ModeComboBox.addItems(infomation)
        self.ModeComboBox.currentIndexChanged.connect(self.Mode)
        self.Mode()
        self.OpenButton.clicked.connect(self.openFileNameDialog)
        self.SaveButton.clicked.connect(self.saveFileDialog)
        self.RunButton.clicked.connect(self.Run)
        self.OpenMenu.triggered.connect(self.openFileNameDialog)
        self.SaveMenu.triggered.connect(self.saveFileDialog)
        
    # 開啟檔案按鈕
    def openFileNameDialog(self):
        self.FilePath, _ = QFileDialog.getOpenFileName(self,"Select image", "","Image File (*.jpeg *.jpg *png);;Python Files (*.py)")
        if self.FilePath:
            print("圖片路徑: ", self.FilePath)
        self.Result.clear()
        self.ResultImageName.clear()
        self.ResultImageSize.clear()
        self.ShowOriginalImage(self.FilePath)
        self.FileName = self.FilePath.split("/")[-1]
        print("檔案名稱: " + self.FileName)
        img = cv2.imread(self.FilePath)
        print("影像大小: " + str(img.shape))
        self.OriginalImageName.setText("Name : " + self.FileName)
        self.OriginalImageSize.setText("Size : " + str(img.shape))

    # 儲存檔案按鈕
    def saveFileDialog(self):
        FilePath, _ = QFileDialog.getSaveFileName(self,"Save image",self.FileName,"Image File (*.jpeg *.jpg *png)")
        if FilePath:
            print("存檔路徑: ", FilePath)
        cv2.imwrite(FilePath, self.open_cv_image)
        self.SaveFileMessage()

    # 顯示原圖
    def ShowOriginalImage(self, FilePath):
        im = QPixmap(FilePath)
        self.Original.setPixmap(im)
        self.RunButton.setEnabled(True)

    # 執行拼接
    def Run(self):
        self.progressBar.show()
        self.RunButton.setEnabled(False)
        self.ResultImage = RunTextureSynthesis(self.FilePath, self.mode)
        # for i in range(100):
        #     time.sleep(500)
        #     self.progressBar.setValue(i)
        self.ShowResultImage()
        self.RunButton.setEnabled(True)

    # 將影像轉換成opencv格式
    def ShowResultImage(self):
        open_cv_image = np.array(self.ResultImage)
        self.open_cv_image = open_cv_image[:, :, ::-1].copy()
        self.qImg = QImage(self.open_cv_image.data, self.open_cv_image.shape[1], self.open_cv_image.shape[0], QImage.Format_RGB888).rgbSwapped()
        self.Result.setPixmap(QPixmap.fromImage(self.qImg))
        self.SaveButton.setEnabled(True)
        self.ResultImageName.setText("Name : " + self.FileName)
        self.ResultImageSize.setText("Size : " + str(self.open_cv_image.shape))

    def Mode(self):
        print("模式選擇: ", self.ModeComboBox.currentText())
        if self.ModeComboBox.currentText() == "週期影像":
            self.mode = 'regular'
        if self.ModeComboBox.currentText() == "非週期影像":
            self.mode = 'random'
        # if self.ModeComboBox.currentText() == "非週期影像":
        #     self.WarnMessage()


    # 對話框
    def SaveFileMessage(self):
        QMessageBox.about(self,"儲存檔案","檔案已儲存成功")

    # 對話框
    def WarnMessage(self):
        QMessageBox.warning(self,"Warning","功能尚未完成")

if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        window = MainWindow()
        window.show()
        sys.exit(app.exec_())
    except KeyboardInterrupt:
        destory()