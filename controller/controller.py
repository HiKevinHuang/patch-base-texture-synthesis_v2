from Model.PatchBasedTextureSynthesis import *
import time


def RunTextureSynthesis(exampleMapPath, mode, outputPath):
    #PARAMS
    # exampleMapPath = "imgs/S__138305570.jpg"
    # outputPath = "./Result/"
    patchSize = 50 #size of the patch (without the overlap)
    overlapSize = 10 #the width of the overlap region
    outputSize = [800,800]

    start = time.time()
    
    pbts = PatchBasedTextureSynthesis(exampleMapPath, outputPath, outputSize, patchSize, overlapSize, mode, in_windowStep = 5, in_mirror_hor = True, in_mirror_vert = True, in_shapshots = False)
    ResultImage = pbts.resolveAll()

    end = time.time()
    CostTime = (end - start)

    TextFileName = exampleMapPath.split("/")[-1].split(".")[0]
    with open(outputPath + TextFileName + '.txt', 'a') as lead: 
        lead.write("\nTime: " + str(round(CostTime)) + " (sec)")

    return ResultImage