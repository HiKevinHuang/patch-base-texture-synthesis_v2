### 開發環境
---
* Python = 3.9
* Cuda version 10.1
* OpevCV = 4.4.0.46
* Numpy = 1.19.2
* PyQt5 = 5.15.2
* cupy-cuda101 = 8.0.0
* Pillow = 8.0.0
* Scikit-image = 0.17.2
* Scikit-learn = 0.23.2
* tqdm = 4.60.0

### 主程式
---
開啟主程式指令
```
$ python app.py
```


