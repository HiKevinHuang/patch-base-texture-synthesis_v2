import os
import time
import cv2
import cupy as cp
import numpy as np
from tqdm import tqdm
from skimage import io
from random import randint
from Model.min_cut import min_cut
from math import floor, ceil
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from sklearn.neighbors import KDTree
from skimage.util.shape import view_as_windows




class PatchBasedTextureSynthesis:
    def __init__(self, originalImage, in_outputPath, in_outputSize, in_patchSize, in_overlapSize, mode, in_windowStep = 5, in_mirror_hor = True, in_mirror_vert = True, in_shapshots = True):
        self.mode = mode
        print("已確定模式: ", self.mode)
        self.FileName = originalImage.split("/")[-1]
        self.exampleMap = self.loadExampleMap(originalImage) # 讀圖
        self.snapshots = in_shapshots
        self.outputPath = in_outputPath     # 輸出路徑
        self.outputSize = in_outputSize     # 輸出尺寸
        self.patchSize = in_patchSize       # 每個區塊的尺寸
        self.overlapSize = in_overlapSize   # 重疊區域的尺寸
        self.mirror_hor = in_mirror_hor     # 水平鏡像
        self.mirror_vert = in_mirror_vert   # 垂直鏡像
        self.total_patches_count = 0        # excluding mirrored versions
        self.windowStep = 5
        self.iter = 0

        self.checkIfDirectoryExists() # 確認輸出路徑存不存在
        self.examplePatches = self.prepareExamplePatches()
        self.canvas, self.filledMap, self.idMap = self.initCanvas()
        self.initFirstPatch() #place random block to start with

        self.kdtree_topOverlap, self.kdtree_leftOverlap, self.kdtree_combined = self.initKDtrees()

        self.PARM_truncation = 0.8
        self.PARM_attenuation = 2



    # (第二步)
    # 確認輸出路徑存不存在
    def checkIfDirectoryExists(self):
        if not os.path.exists(self.outputPath):
            os.makedirs(self.outputPath)


    # 儲存參數
    def saveParams(self):
        #write Params
        TextFileName = self.FileName.split(".")[0]
        text_file = open(self.outputPath + TextFileName + '.txt', "w")
        text_file.write("File Name: %s \nMode: %s \nPatchSize: %d \nOverlapSize: %d \nMirror Vert: %d \nMirror Hor: %d \nOutput Size: %dx%d" % (self.FileName, self.mode, self.patchSize, self.overlapSize, self.mirror_vert, self.mirror_hor, self.outputSize[0], self.outputSize[1]))
        text_file.close()


    # 解決那些還沒解決的區塊
    def resolveAll(self):
        self.saveParams()

        #resolve all unresolved patches
        for i in tqdm(range(int(cp.sum(1-self.filledMap)))):
            self.resolveNext()
            
        # 只存最終結果圖
        if not self.snapshots:
            self.canvas = cp.asnumpy(self.canvas)
            img = Image.fromarray(np.uint8(self.canvas*255))
            img = img.resize((self.outputSize[0], self.outputSize[1]), resample=0, box=None)
            img.save(self.outputPath + self.FileName)
        else:
            self.visualize([0,0], [], [], showCandidates=False)
        
        return img


    # 處理下一個區塊
    def resolveNext(self):
        
        #coordinate of the next one to resolve
        coord = self.idCoordTo2DCoord(cp.sum(self.filledMap), cp.shape(self.filledMap)) #get 2D coordinate of next to resolve patch
        #get overlap areas of the patch we want to resolve
        overlapArea_Top = self.getOverlapAreaTop(coord)
        overlapArea_Left = self.getOverlapAreaLeft(coord)
        
        #find most similar patch from the examples
        dist, ind = self.findMostSimilarPatches(overlapArea_Top, overlapArea_Left, coord)
        
        if self.mirror_hor or self.mirror_vert:
            #check that top and left neighbours are not mirrors
            dist, ind = self.checkForMirrors(dist, ind, coord)

        if (len(dist) == 0):
            dist, ind = self.findMostSimilarPatches(overlapArea_Top, overlapArea_Left, coord)

        #choose random valid patch
        probabilities = self.distances2probability(dist, self.PARM_truncation, self.PARM_attenuation)
        # print("ind: ", ind)
        # print("probabilities final: ", probabilities)
        chosenPatchId = cp.random.choice(ind, 1, p=probabilities)
        
        #update canvas
        blend_top = (overlapArea_Top is not None)
        blend_left = (overlapArea_Left is not None)
        self.updateCanvas(chosenPatchId[0], coord[0], coord[1], blend_top, blend_left)
        
        #update filledMap and id map ;)
        self.filledMap[coord[0], coord[1]] = 1

        self.idMap[coord[0], coord[1]] = chosenPatchId[0]
        
        #visualize
        self.visualize(coord, chosenPatchId, ind)
        
        self.iter += 1
        # print(self.iter)



    # (第三步)
    # 列出候選區塊
    def prepareExamplePatches(self):
        searchKernelSize = self.patchSize + 2 * self.overlapSize # 定義區塊大小

        # exampleMap :要切割的數據；window_shape:切割窗口大小；windowStep：窗口移動的步幅
        result = view_as_windows(self.exampleMap, [searchKernelSize, searchKernelSize, 3] , self.windowStep)
        shape = cp.shape(result)
        result = result.reshape(shape[0]*shape[1], searchKernelSize, searchKernelSize, 3)
        result = cp.asarray(result)
        
        self.total_patches_count = shape[0]*shape[1]

        if self.mirror_hor:
            #flip along horizonal axis
            hor_result = cp.zeros(cp.shape(result))
            
            for i in range(self.total_patches_count):
                hor_result[i] = result[i][::-1, :, :]
            
            result = cp.concatenate((result, hor_result))
        if self.mirror_vert:
            vert_result = cp.zeros((shape[0]*shape[1], searchKernelSize, searchKernelSize, 3))
            
            for i in range(self.total_patches_count):
                vert_result[i] = result[i][:, ::-1, :]
            # print(cp.shape(result))
            # print(cp.shape(vert_result))
            result = cp.concatenate((result, vert_result))
        return result


    # (第四步)
    def initCanvas(self):
        # 確認總共需要多少塊
        # check whether the outputSize adheres to patch+overlap size
        num_patches_X = ceil((self.outputSize[0]-self.overlapSize)/(self.patchSize+self.overlapSize))
        num_patches_Y = ceil((self.outputSize[1]-self.overlapSize)/(self.patchSize+self.overlapSize))

        # 計算輸出的圖片大小
        # calc needed output image size
        required_size_X = num_patches_X*self.patchSize + (num_patches_X+1)*self.overlapSize
        required_size_Y = num_patches_Y*self.patchSize + (num_patches_X+1)*self.overlapSize
        
        # create empty canvas
        canvas = cp.zeros((required_size_X, required_size_Y, 3)) # 建立黑色底圖
        filledMap = cp.zeros((num_patches_X, num_patches_Y))     # map showing which patches have been resolved # 用於顯示有幾個區塊已經處理過了
        idMap = cp.zeros((num_patches_X, num_patches_Y)) - 1     # stores patches id 用於儲存區塊的ID
        
        # print("modified output size: ", cp.shape(canvas))
        # print("number of patches: ", cp.shape(filledMap)[0])

        return canvas, filledMap, idMap


    # (第五步)
    # 選出第一個區塊當作第一塊
    def initFirstPatch(self):
        # 隨機選取一塊
        # grab a random block 
        patchId = randint(0, cp.shape(self.examplePatches)[0])

        # 將 fillmap 的第一塊標記成 1 (表示已處理)
        # mark out fill map
        self.filledMap[0, 0] = 1
        self.idMap[0, 0] = patchId % self.total_patches_count
        # update canvas
        self.updateCanvas(patchId, 0, 0, False, False)
        # visualize
        self.visualize([0,0], [patchId], [])
        self.iter += 1


    def idCoordTo2DCoord(self, idCoord, imgSize):
        row = int(floor(idCoord / imgSize[0]))
        col = int(idCoord - row * imgSize[1])
        return [row, col]


    # (第六步)
    def updateCanvas(self, inputPatchId, coord_X, coord_Y, blendTop = False, blendLeft = False):
        #translate Patch coordinate into Canvas coordinate
        x_range = self.patchCoord2canvasCoord(coord_X)
        y_range = self.patchCoord2canvasCoord(coord_Y)

        examplePatch = cp.array(self.examplePatches[inputPatchId])
        if blendLeft:
            canvasOverlap = self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[0]+self.overlapSize]
            examplePatchOverlap = cp.copy(examplePatch[:, 0:self.overlapSize])
            if self.mode == 'random':
                examplePatch[:, 0:self.overlapSize] = self.linearBlendOverlaps(canvasOverlap, examplePatchOverlap, 'left')
            else:
                examplePatch[:, 0:self.overlapSize] = self.MinCut(canvasOverlap, examplePatchOverlap, self.canvas[x_range[0]:x_range[1], x_range[0]:x_range[1]], examplePatch, 1)
                # examplePatchOverlapB = cp.asnumpy(examplePatch[:, 0:self.overlapSize])
                # plt.imshow(examplePatchOverlapB)
                # plt.show()
            # plt.imshow(cp.asnumpy(canvasOverlap))
            # plt.imshow(cp.asnumpy(examplePatchOverlap))
            # plt.imshow(cp.asnumpy(self.canvas[x_range[0]:x_range[1], x_range[0]:x_range[1]]))
            # plt.imshow(cp.asnumpy(examplePatch))
            # plt.show()
            # 
            # plt.imshow(cp.asnumpy(examplePatch[:, 0:self.overlapSize]))
            # plt.show()
            
        if blendTop:
            canvasOverlap = self.canvas[x_range[0]:x_range[0]+self.overlapSize, y_range[0]:y_range[1]]
            examplePatchOverlap = cp.copy(examplePatch[0:self.overlapSize, :])
            if self.mode == 'random':
                examplePatch[0:self.overlapSize, :] = self.linearBlendOverlaps(canvasOverlap, examplePatchOverlap, 'top')
            else:
                examplePatch[0:self.overlapSize, :] = self.MinCut(canvasOverlap, examplePatchOverlap, self.canvas[x_range[0]:x_range[1], x_range[0]:x_range[1]], examplePatch, 2)
                # examplePatchOverlapA = cp.asnumpy(examplePatch[0:self.overlapSize, :])
                # plt.imshow(examplePatchOverlapA)
                # plt.show()
            # plt.imshow(cp.asnumpy(canvasOverlap))
            # plt.imshow(cp.asnumpy(examplePatchOverlap))
            # plt.imshow(cp.asnumpy(self.canvas[x_range[0]:x_range[1], x_range[0]:x_range[1]]))
            # plt.imshow(cp.asnumpy(examplePatch))
            # plt.show()
            # 
            # plt.imshow(cp.asnumpy(examplePatch[0:self.overlapSize, :]))
            # plt.show()
        # cv2.imshow("canvas", cp.asnumpy(self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]]))
        # cv2.imshow("examplePatch", cp.asnumpy(examplePatch))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]] = examplePatch
        # plt.imshow(cp.asnumpy(self.canvas))
        # plt.imshow(cp.asnumpy(self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]]))
        # plt.show()
        # print(cp.shape(self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]]))
        # print(self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]])


    # 1:左右  2:上下
    def MinCut(self, canvasOverlap, examplePatchOverlap, image_left, image_right, direction):

        image_left1 = cp.asnumpy(canvasOverlap)
        image_right1 = cp.asnumpy(examplePatchOverlap)

        mask = min_cut(image_left1, image_right1, self.overlapSize , 10, direction, self.outputPath)
        # plt.imshow(mask)
        # plt.show()

        # cv2.imshow('mask', mask)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        mask = cp.asarray(mask)
        # print(mask.shape)
        # print(mask)
        # print(canvasOverlap.shape)
        # plt.imshow(cp.asnumpy(mask))
        # plt.show()
        # maskk = (canvasOverlap * mask) + (examplePatchOverlap * (1 - mask))


        return mask
        # return maskk

    # 產生 alpha blend 遮罩
    def linearBlendOverlaps(self, canvasOverlap, examplePatchOverlap, mode):
        if mode == 'left':
            mask = cp.repeat(cp.arange(self.overlapSize)[cp.newaxis, :], cp.shape(canvasOverlap)[0], axis=0) / self.overlapSize
        elif mode == 'top':
            mask = cp.repeat(cp.arange(self.overlapSize)[:, cp.newaxis], cp.shape(canvasOverlap)[1], axis=1) / self.overlapSize
        mask = cp.repeat(mask[:, :, cp.newaxis], 3, axis=2) #cast to 3d array

        return canvasOverlap * (1 - mask) + examplePatchOverlap * mask
        # return canvasOverlap * mask + examplePatchOverlap * (1 - mask)
    

    # (第八步)
    # 顯示圖片
    def visualize(self, coord, chosenPatchId, nonchosenPatchId, showCandidates = True):
        #full visualization includes both example and generated img
        canvasSize = cp.shape(self.canvas)
        #insert generated image
        vis = cp.zeros((canvasSize[0], canvasSize[1], 3)) + 0.2
        vis[:, 0:canvasSize[1]] = self.canvas
        #insert example
        exampleHighlited = cp.copy(self.exampleMap)
        if showCandidates:
            exampleHighlited = self.hightlightPatchCandidates(chosenPatchId, nonchosenPatchId)
        h = floor(canvasSize[0] / 2)
        w = floor(canvasSize[1] / 2)
        exampleResized = self.resize(exampleHighlited, [h, w])
        offset_h = floor((canvasSize[0] - h) / 2) 
        offset_w = floor((canvasSize[1] - w) / 2)
        
        # 右邊放置原始圖片
        # vis[offset_h:offset_h+h, canvasSize[1]+offset_w:canvasSize[1]+offset_w+w] = exampleResized
        vis = cp.asnumpy(vis)
        #show live update
        # plt.imshow(vis)
        # plt.show()
        # plt.title("image")
        # plt.pause(1)
        # plt.clf()
        # clear_output(wait=True)
        # display(plt.show())

        # 存圖
        if self.snapshots:
            vis = cp.asnumpy(vis)
            img = Image.fromarray(np.uint8(vis*255))
            img = img.resize((self.outputSize[0], self.outputSize[1]), resample=0, box=None)
            img.save(self.outputPath + 'out' + str(self.iter) + '.jpg')


    # (第九步)
    def hightlightPatchCandidates(self, chosenPatchId, nonchosenPatchId):
        result = cp.copy(self.exampleMap)
        result = cp.array(result)
        #mod patch ID
        chosenPatchId = chosenPatchId[0] % self.total_patches_count
        if len(nonchosenPatchId)>0:
            nonchosenPatchId = nonchosenPatchId % self.total_patches_count
            #exlcude chosen from nonchosen
            chosenPatchId = cp.asnumpy(chosenPatchId)
            nonchosenPatchId = np.delete(nonchosenPatchId, np.where(nonchosenPatchId == chosenPatchId))
            nonchosenPatchId = cp.asarray(nonchosenPatchId)
            #highlight non chosen candidates
            c = [0.25, 0.9 ,0.45]
            self.highlightPatches(result, nonchosenPatchId, color=c, highlight_width = 4, alpha = 0.5)
        
        #hightlight chosen
        c = [1.0, 0.25, 0.15]
        self.highlightPatches(result, [chosenPatchId], color=c, highlight_width = 4, alpha = 1)

        return result


    # 用顏色標示出匹配的圖片
    def highlightPatches(self, writeResult, patchesIDs, color, highlight_width = 2, solid = False, alpha = 0.1):
        
        searchWindow = self.patchSize + 2*self.overlapSize
        #number of possible steps
        row_steps = floor((cp.shape(writeResult)[0] - searchWindow) / self.windowStep) + 1
        col_steps = floor((cp.shape(writeResult)[1] - searchWindow) / self.windowStep) + 1 
        
        for i in range(len(patchesIDs)):
            
            chosenPatchId = patchesIDs[i]
            
            #patch Id to step
            patch_row = floor(chosenPatchId / col_steps)
            patch_col = chosenPatchId - patch_row * col_steps
            
            #highlight chosen patch (below are boundaries of the example patch)
            row_start = self.windowStep* patch_row
            row_end = self.windowStep * patch_row + searchWindow
            col_start = self.windowStep * patch_col
            col_end = self.windowStep * patch_col + searchWindow
            
            if not solid:
                w = highlight_width
                color = cp.asarray(color)
                overlap = cp.copy(writeResult[row_start:row_start+w, col_start:col_end])
                writeResult[row_start:row_start+w, col_start:col_end] = overlap * (1-alpha) + (cp.zeros(cp.shape(overlap))+color) * alpha #top
                overlap = cp.copy(writeResult[row_end-w:row_end, col_start:col_end])
                writeResult[row_end-w:row_end, col_start:col_end] = overlap * (1-alpha) + (cp.zeros(cp.shape(overlap))+color) * alpha #bot
                overlap = cp.copy( writeResult[row_start:row_end, col_start:col_start+w])
                writeResult[row_start:row_end, col_start:col_start+w] = overlap * (1-alpha) + (cp.zeros(cp.shape(overlap))+color) * alpha #left
                overlap = cp.copy(writeResult[row_start:row_end, col_end-w:col_end])
                writeResult[row_start:row_end, col_end-w:col_end] = overlap * (1-alpha) + (cp.zeros(cp.shape(overlap))+color) * alpha #end
            else:
                a = alpha
                writeResult[row_start:row_end, col_start:col_end] =  writeResult[row_start:row_end, col_start:col_end] * (1-a) + (cp.zeros(cp.shape(writeResult[row_start:row_end, col_start:col_end]))+color) * a

    # (第十步)
    def resize(self, imgArray, targetSize):
        imgArray = cp.asnumpy(imgArray)
        img = Image.fromarray(np.uint8(imgArray*255))
        img = img.resize((targetSize[0], targetSize[1]), resample=0, box=None)
        img = cp.asarray(img)
        return cp.array(img)/255


    # 找到最相似的區塊圖片
    def findMostSimilarPatches(self, overlapArea_Top, overlapArea_Left, coord, in_k=5):

        overlapArea_Top = cp.asnumpy(overlapArea_Top)
        overlapArea_Left = cp.asnumpy(overlapArea_Left)

        #check which KD tree we need to use
        if (str(overlapArea_Top) != 'None') and (str(overlapArea_Left) != 'None'):
            combined = self.getCombinedOverlap(overlapArea_Top.reshape(-1), overlapArea_Left.reshape(-1))
            combined = cp.asnumpy(combined)
            dist, ind = self.kdtree_combined.query([combined], k=in_k)
        elif (str(overlapArea_Top) != 'None'):
            dist, ind = self.kdtree_topOverlap.query([overlapArea_Top.reshape(-1)], k=in_k)
        elif (str(overlapArea_Left) != 'None'):
            dist, ind = self.kdtree_leftOverlap.query([overlapArea_Left.reshape(-1)], k=in_k)
        else:
            raise Exception("ERROR: no valid overlap area is passed to -findMostSimilarPatch-")
        dist = dist[0]
        ind = ind[0]
        
        return dist, ind
     
    #disallow visually similar blocks to be placed next to each other
    def checkForMirrors(self, dist, ind, coord, thres = 3):
        remove_i = []
        #do I have a top or left neighbour
        if coord[0]-1>-1:
            top_neigh = int(self.idMap[coord[0]-1, coord[1]])
            for i in range(len(ind)): 
                if (abs(ind[i]%self.total_patches_count - top_neigh%self.total_patches_count) < thres):
                    remove_i.append(i)     
        if  coord[1]-1>-1:
            left_neigh = int(self.idMap[coord[0], coord[1]-1])
            for i in range(len(ind)):
                if (abs(ind[i]%self.total_patches_count - left_neigh%self.total_patches_count) < thres):
                    remove_i.append(i)  
        
        dist = np.delete(dist, remove_i)
        ind = np.delete(ind, remove_i)
        
        return dist, ind

        
    def distances2probability(self, distances, PARM_truncation, PARM_attenuation):
        # print("distances: ", distances)
        probabilities = 1 - distances / np.max(distances)  
        probabilities *= (probabilities > PARM_truncation)
        probabilities = pow(probabilities, PARM_attenuation) #attenuate the values
        # print("probabilities before: ", probabilities)
        #check if we didn't truncate everything!
        if np.sum(probabilities) == 0:
            #then just revert it
            probabilities = 1 - distances / np.max(distances) 
            probabilities *= (probabilities > PARM_truncation*np.max(probabilities)) # truncate the values (we want top truncate%)
            probabilities = pow(probabilities, PARM_attenuation)
        # print("probabilities after: ", probabilities)
        probabilities /= np.sum(probabilities) #normalize so they add up to one 
        # print("check: ", probabilities)
        # print(np.isnan(probabilities[0]))
        if np.isnan(probabilities[0]):
            print(".")
            probabilities[0] = 1
        else:
            pass

        return probabilities

        
    def getOverlapAreaTop(self, coord):
        #do I have a top neighbour
        if coord[0]-1>-1:
            canvasPatch = self.patchCoord2canvasPatch(coord)
            return canvasPatch[0:self.overlapSize, :, :]
        else:
            return None
        
    def getOverlapAreaLeft(self, coord):
        #do I have a left neighbour
        if coord[1]-1>-1:
            canvasPatch = self.patchCoord2canvasPatch(coord)
            return canvasPatch[:, 0:self.overlapSize, :]    
        else:
            return None 
 
    def initKDtrees(self):
        print("prepate overlap patches...")
        #prepate overlap patches
        topOverlap = self.examplePatches[:, 0:self.overlapSize, :, :]
        leftOverlap = self.examplePatches[:, :, 0:self.overlapSize, :]
        shape_top = cp.shape(topOverlap)
        shape_left = cp.shape(leftOverlap)

        flatten_top = topOverlap.reshape(shape_top[0], -1)
        flatten_left = leftOverlap.reshape(shape_left[0], -1)
        flatten_combined = self.getCombinedOverlap(flatten_top, flatten_left) 
        flatten_top = cp.asnumpy(flatten_top)
        flatten_left = cp.asnumpy(flatten_left)
        flatten_combined = cp.asnumpy(flatten_combined)

        tree_top = KDTree(flatten_top)
        tree_left = KDTree(flatten_left)
        tree_combined = KDTree(flatten_combined)

        flatten_top = cp.asarray(flatten_top)
        flatten_left = cp.asarray(flatten_left)
        flatten_combined = cp.asarray(flatten_combined)
       
        return tree_top, tree_left, tree_combined
    

    #the corner of 2 overlaps is counted double
    def getCombinedOverlap(self, top, left):
        top = cp.asarray(top)
        left = cp.asarray(left)
        shape = cp.shape(top)

        if len(shape) > 1:
            combined = cp.zeros((shape[0], shape[1]*2))
            combined[0:shape[0], 0:shape[1]] = top
            combined[0:shape[0], shape[1]:shape[1]*2] = left
        else:
            combined = cp.zeros((shape[0]*2))
            combined[0:shape[0]] = top
            combined[shape[0]:shape[0]*2] = left
        return combined


    # (第七步)
    def patchCoord2canvasCoord(self, coord):
        return [(self.patchSize+self.overlapSize)*coord, (self.patchSize+self.overlapSize)*(coord+1) + self.overlapSize]


    def patchCoord2canvasPatch(self, coord):
        x_range = self.patchCoord2canvasCoord(coord[0])
        y_range = self.patchCoord2canvasCoord(coord[1])
        return cp.copy(self.canvas[x_range[0]:x_range[1], y_range[0]:y_range[1]])
        

    # (第一步)
    # 讀圖
    def loadExampleMap(self, originalImage):
        exampleMap = io.imread(originalImage) # 回傳 MxNx3 矩陣
        exampleMap = exampleMap / 255.0 # 正規化 0~1

        # 確保是三通道
        # 若為四通道，就移除 Alpha 通道
        # 若為灰階二通道，就轉成 RGB 三通道
        if (cp.shape(exampleMap)[-1] > 3): 
            exampleMap = exampleMap[:,:,:3]
        elif (len(cp.shape(exampleMap)) == 2):
            exampleMap = cp.repeat(exampleMap[cp.newaxis, :, :], 3, axis=0) # convert from Grayscale to RGB
        return exampleMap