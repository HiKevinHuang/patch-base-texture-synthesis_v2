from controller.controller import RunTextureSynthesis
from Model.min_cut import min_cut
import cv2
import numpy as np
import matplotlib.pyplot as plt



# pathA = "./imgs/RockImage/28/outImage_ 0.jpg"
# pathB = "./imgs/RockImage/28/outImage_ 1.jpg"
# overlap = 30
# filter_size = 30
# direction = 2
# savefile_path = "./"
# pathA = cv2.imread(pathA).astype('float')
# pathB = cv2.imread(pathB).astype('float')
# h, w, c = pathA.shape

# mask_up = min_cut(pathA, pathB, overlap, filter_size, direction, savefile_path)

# if direction == 1:
#     im_up = cv2.hconcat([pathA[:,0:w-overlap,:], mask_up])
#     im_up = cv2.hconcat([im_up, pathB[:,overlap:w,:]])
# else:
#     im_dn = cv2.vconcat([pathA[0:h-overlap,:,:], mask_up])
#     im_dn = cv2.vconcat([im_dn, pathB[overlap:w,:,:]])

# plt.figure()
# plt.imshow(pathA/255)
# plt.figure()
# plt.imshow(pathB/255)
# plt.figure()
# plt.imshow(mask_up/255)
# plt.figure()
# plt.imshow(im_dn/255)
# plt.show()


# =========================================================================================

# exampleMapPath = "./imgs/t28.jpg"
exampleMapPath = "./imgs/RockImage/3/Image_ 0.jpg"
# outputPath = "./imgs/regular800x800/"
outputPath = "./imgs/random800x800/"
mode = "random" # use alpha blend 
# mode = "regular"  # use min cut
ResultImage = RunTextureSynthesis(exampleMapPath, mode, outputPath)
# print(ResultImage)
# plt.imshow(ResultImage)
# plt.show()
# cv2.imwrite("./out.jpg", ResultImage)
# cv2.imshow("main",ResultImage)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# =========================================================================================

# # 創建重疊區域空白圖，並轉為三通道
# mix_up = np.zeros(mask_up.shape)
# mix_up = np.expand_dims(mix_up, axis=2)
# mix_up = np.concatenate((mix_up, mix_up, mix_up), axis=-1)

# # 左圖 + 右圖融合
# imA = cv2.imread(pathA)
# imB = cv2.imread(pathB)
# h, w, c = imA.shape  # 圖片高、寬、通道
# mix = overlap
# for ch in range(3):
#     mix_up[:,:,ch] = (mask_up * imA[:, w-mix:w, ch]) + ((1-mask_up) * imB[:, 0:mix, ch])

# print(mix_up)

# cv2.imshow("main",mask_up/255)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# %%
